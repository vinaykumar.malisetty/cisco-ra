FROM artifactory.ciena.com/blueplanet/base-image-devops-toolkit:20200211-py3.7

WORKDIR /bp2/src/

ARG PYPI='https://artifactory.ciena.com/api/pypi/blueplanet-ubuntu-bionic-pypi/simple'

COPY .devops-toolkit /bp2/.devops-toolkit
COPY requirements.txt /bp2/src/

RUN pip install --find-links /bp2/.devops-toolkit -r requirements.txt

COPY . /bp2/src/

RUN pip install --find-links /bp2/.devops-toolkit -e .

RUN mkdir -p /bp2/log

EXPOSE 8080

ENV RASDK_SETTINGS_MODULE=bpracisco.settings \
    SBIS=kafka,statsd,graphite,bpocore,cassandra3,zookeeper,gcs \
    BP2HOOK=http://{ip}:8888/bp2-hook

# Configure git user
RUN ssh-keygen -t rsa -f /root/.ssh/id_rsa -q -N "" && \
    git config --global user.email "bpracisco@ciena.com" && \
    git config --global user.name "bpracisco"

CMD ["bpracisco", "--logfile", "/bp2/log/bpracisco.log", "--kafka"]