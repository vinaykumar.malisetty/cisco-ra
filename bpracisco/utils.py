import json


def print_data(data):
    import logging
    log = logging.getLogger(__name__)
    log.info("data is: {}".format(json.dumps(data)))
    return data


def merge_record(data):
    record = []
    used_ids = []

    for i in data['isis']:
        record_id = i['id']
        if record_id not in used_ids:
            record.append(i)
            used_ids.append(record_id)
        else:
            for item in record:
                if item['id'] == record_id:
                    item['admin_state'] = i['admin_state']
                    item['operational_state'] = i['operational_state']
                    item['circuit_type'] = i['circuit_type']
    return record


def arrange_policy_map_9k(data):
    policy_map = {}
    for policy in data:
        name = policy['policyName']
        if name in policy_map:
            policy_map[name].append(policy)
        else:
            policy_map[name] = [policy]
    temp_policy_map = {}
    for name, details in policy_map.items():
        policy_map_new = {"classmap": {}}
        class_name = ""
        for entry in details:
            if entry.get("description"):
                policy_map_new["description"] = entry["description"]
            if entry.get("classMap"):
                class_name = entry["classMap"]
                policy_map_new["classmap"][class_name] = {}
            if entry.get("eir") and entry.get("unit"):
                if entry.get("unit") == "kbps":
                    policy_map_new["classmap"][class_name]["eir"] = int(entry["eir"]) * 1024
                if entry.get("unit") == "mbps":
                    policy_map_new["classmap"][class_name]["eir"] = int(entry["eir"]) * 1048576
                if entry.get("unit") == "gbps":
                    policy_map_new["classmap"][class_name]["eir"] = int(entry["eir"]) * 1073741824
            if entry.get("cir"):
                policy_map_new["classmap"][class_name]["cir"] = entry["cir"]
            if entry.get("ebs"):
                policy_map_new["classmap"][class_name]["ebs"] = entry["ebs"]
            if entry.get("cbs"):
                policy_map_new["classmap"][class_name]["cbs"] = entry["cbs"]
            if entry.get("priority"):
                policy_map_new["classmap"][class_name]["priority"] = entry["priority"]
            if entry.get("shaper"):
                policy_map_new["classmap"][class_name]["shaper"] = entry["shaper"]
            if entry.get("cos"):
                policy_map_new["classmap"][class_name]["cos"] = entry["cos"]
            if entry.get("qos"):
                policy_map_new["classmap"][class_name]["qos"] = entry["qos"]
            if entry.get("bandwidth"):
                policy_map_new["classmap"][class_name]["bandwidth"] = entry["bandwidth"]
            if entry.get("burstSize"):
                policy_map_new["classmap"][class_name]["burstSize"] = entry["burstSize"]
            if entry.get("exceedAction"):
                policy_map_new["classmap"][class_name]["exceed-action"] = entry["exceedAction"]
            if entry.get("conformAction"):
                policy_map_new["classmap"][class_name]["conform-action"] = entry["conformAction"]
            if entry.get("mplsexp"):
                policy_map_new["classmap"][class_name]["mplsexp"] = entry["mplsexp"]
            if entry.get("dscp"):
                try:
                    policy_map_new["classmap"][class_name]["dscp"] = int(entry["dscp"])
                except ValueError:
                    policy_map_new["classmap"][class_name]["dscp"] = entry["dscp"]
            if entry.get("ccos"):
                policy_map_new["classmap"][class_name]["ccos"] = entry["ccos"]
            if entry.get("cqos"):
                policy_map_new["classmap"][class_name]["cqos"] = entry["cqos"]
        temp_policy_map[name] = policy_map_new

    final_policy_map = {"policy_map": temp_policy_map}
    #  log.info("ASR9k PolicyMap:%s", json.dumps(final_policy_map))
    return final_policy_map
