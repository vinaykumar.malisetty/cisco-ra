'''
#
# Module: netconf-tools.py
# Descr : Collection of list based translators for removing elements
#         from a list.
#
# Copyright(c) 2014, Cyan, Inc. All rights reserved.
#
'''

import os
from bpprov.translators.base import Translator


def listDataValid(listdata, index):
    return isinstance(listdata, list) and 0 <= index < len(listdata)


def dictGetPath(data, path):
    path = path.replace("{", "").replace("}", "").replace(" ", "")
    for entry in path.split("."):
        if entry == "data":
            continue
        data = data[entry]

    return data


class PopElement(Translator):
    schema_base = os.path.dirname(os.path.abspath(__file__))
    schema = 'list-pop-index.json'

    def process(self, route_data):
        index = self.params['index']

        if not listDataValid(route_data.data, index):
            route_data.data = {}
            return route_data

        route_data.data = route_data.data.pop(index)
        return route_data


class PopElementByKey(Translator):
    schema_base = os.path.dirname(os.path.abspath(__file__))
    schema = 'list-pop-dict.json'

    def process(self, route_data):
        key = self.params['key']
        value = self.params['value']
        set_terminated_on_missing = True
        if "set_terminated_on_missing" in self.params:
            set_terminated_on_missing = self.params['set_terminated_on_missing']

        data = route_data.data

        if "{{" in value:
            value = dictGetPath(data, value)

        if isinstance(data, dict) and "versa_data" in data:
            data = data["versa_data"]
        elif isinstance(data, dict) and "data_dict" in data:
            data = data["data_dict"]

        print("[PopElementByKey] KEY: " + str(key) + ", VALUE: " + str(value) + ", DATA: " + str(data))

        newData = []
        if isinstance(data, list):
            for data_dict in data:
                failure = False
                test_dict = data_dict
                for part_key in key.split("."):
                    if part_key in test_dict:
                        test_dict = test_dict[part_key]
                    else:
                        failure = True
                        continue

                if not failure and test_dict == value:
                    newData.append(data_dict)
                    break
                else:
                    continue
        else:
            print("[PopElementByKey] ERROR: No list found in data")

        if len(newData) == 0 and set_terminated_on_missing:
            # Get the parameter that needs to be set
            for k, v in route_data.data.items():
                if not isinstance(v, list) and not isinstance(v, dict):
                    break

                obj = {
                    "orchState": "terminated",
                    "properties": {
                        k: v
                    }
                }
                newData.append(obj)

        route_data.data = newData

        print("[PopElementByKey] Output: " + str(route_data))
        return route_data
