import logging
import os
from bpprov.translators.base import Translator
from bpprov.runners.exceptions.base import CatchBase
from bpprov.components.base import RouteData
from bpprov.utils import import_class
from bpracisco import settings
from twisted.internet import defer
from collections import defaultdict

log = logging.getLogger(__name__)


@defer.inlineCallbacks
def run_command(config, params, route_data, trace_id):
    command = params.get('command', None)
    include_header = params.get('include-header', False)
    if not include_header:
        rd = RouteData({}, route_data.data)
    else:
        rd = route_data
        rd.header.pop('runner', None)
    js_file = config.hierarchy.get('commands', command)
    js = config.get_json(js_file)
    runner_cls = import_class(js['type'])
    ep_name = js['endpoint']
    endpoint = config.session.get_endpoint(ep_name)
    yield config.vm(
        'frame', 'push',
        runner=js, route_data=rd, runner_file=js_file, dir=runner_cls.initial_dir, trace_id=trace_id)
    result = yield runner_cls(endpoint, config, js).run(rd)
    yield config.vm('frame', 'pop', trace_id=trace_id)
    defer.returnValue(result)


class Catch(CatchBase):
    schema_base = os.path.join(settings.BPPROV_MODEL_DIRPATH, 'schema')
    schema = 'exceptions/catch.json'

    @defer.inlineCallbacks
    def rcatch(self, route_data, exception):
        result = yield run_command(self.config, self.params, route_data, self.runner.trace_id)
        defer.returnValue(result)


class SessionId(Translator):
    schema = "device.json"
    schema_base = os.path.join(os.path.dirname(os.path.abspath(__file__)), "schema")
    documentation = {
        'overview': (
            'Add the session_id of the running pipeline.  The data must already be a dict, '
            'the attribute "session" is added to the dict, with the sessionId value.'
        ),
        'examples': [{
            'description': 'assume here that the session.id="NE1"',
            'parameters': {},
            'input': {'data': {'a': 1, 'b': 'abc', 'c': 9}},
            'output': {'data': {'session': 'NE1', 'a': 1, 'b': 'abc', 'c': 9}},
        }]
    }

    def process(self, route_data):
        sessionId = self.config.get_session_id()
        route_data.data['sessionId'] = sessionId
        domainId = sessionId.split("_")[1]
        route_data.data['domainId'] = domainId
        return route_data


def change_interface_name(data):
    new_value = ""
    interface_name = data["data.id"]

    if "FRE_" in interface_name:
        fre_data = interface_name.split("_")
        if len(fre_data) == 3:
            new_value = fre_data[2]
        else:
            new_value = fre_data[1]
    else:
        new_value = interface_name

    data['new_value'] = new_value
    return data


def get_interfaces(data):
    return data['interface_list']


def update_data(data):
    service_selector = None
    interface_list = []

    try:
        if 'included' in data['properties'] and data['properties']['included']:
            for entity in data['properties']['included']:
                if 'type' in entity and entity['type'] == 'serviceIntentSpec':
                    if 'attributes' in entity and 'layer3Attributes' in entity['attributes']:
                        if 'serviceType' in entity['attributes']['layer3Attributes'] \
                                and entity['attributes']['layer3Attributes']['serviceType']:
                            service_selector = entity['attributes']['layer3Attributes']['serviceType']
                elif 'data' in entity and entity['data']['type'] == 'endpoints':
                    for tpe in entity['data']['relationships']['tpes']['data']:
                        for location in tpe['attributes']['locations']:
                            interface_name = location['userData']['interface_type']
                            if 'subport' in location:
                                interface_name = interface_name + location['port'] + "." + location['subport']
                            else:
                                interface_name = interface_name + location['port']
                            interface_list.append({"interface_name": interface_name, "properties": location})
    except Exception as ex:
        log.error("Error updating data : {}", ex)

    data['service_selector'] = service_selector
    data['interface_list'] = interface_list
    return data


def interface_vlans_to_Integers(data):
    # print "data is:", json.dumps(data)
    vlans = []
    for vlan in data:
        vlans.append(int(vlan))
    vlans = list(set(vlans))
    return vlans


def update_lag_info_xe(data):
    output = data["output"]
    lag_dict = defaultdict(list)
    for p in output:
        if p[1]:
            new_list = p[1].split(" ")
            for i in new_list:
                lag_dict[p[0]].append(i.split("(")[0])
    data["output"] = lag_dict
    return data


def update_lag_info(data):
    output = data["output"]
    lag_dict = defaultdict(list)
    for p in output:
        if p[1]:
            if p[0] in lag_dict:
                lag_dict[p[0]].append(p[1])
            else:
                lag_dict.update({p[0]: p[1:]})
    data["output"] = lag_dict
    return data


def compare_lists(data):
    output = data["output"]
    new_list = data["properties"]["data"]["attributes"]["layerTerminations"][0]["LinkAggregationPackage"]["allPorts"][:]
    # lag = data["properties"]["data"]["attributes"]["layerTerminations"][0]["LinkAggregationPackage"]["lagId"]
    old_list = []
    for items in output:
        if items["members"] != "":
            old_list.append(items["members"])

    sub = list(set(old_list) - set(new_list))

    data['output'] = {"delete": sub, "add": new_list}
    return data


def get_data(data):
    # data = json.loads(data)
    return data["final_data"]


def update_ipv6_info(data):
    if data["output"]:
        final_list = []
        for i in data["output"]:
            final_list.append(i[0] + "/" + i[1])
        data["properties"]["data"]["attributes"]["layerTerminations"][0]["portAttributesPackage"].update(
            {"ipv6Address": final_list})
    else:
        data["properties"]["data"]["attributes"]["layerTerminations"][0]["portAttributesPackage"].update(
            {"ipv6Address": "unassigned"})
    return data


def return_dict(data):
    return data["data_dict"][0]


def get_VRF(route_data):
    rd = ""
    vrf = ""
    description = ""
    import_route = []
    export_route = []
    interfaces = []
    for i in route_data:
        vrf = i['vrf']
        if i['rd']:
            rd = i['rd']
        if i['description']:
            description = i['description']
        if i['import']:
            import_route.append(i['import'])
        if i['export']:
            export_route.append(i['export'])
        if i['interface']:
            interfaces.append(i['interface'])
    data = {
        "vrfs": {
            "vrf_name": vrf,
            "description": description,
            "rd_value": rd,
            "import_route": import_route,
            "export_route": export_route,
            "interface_list": interfaces
        }
    }
    return data
