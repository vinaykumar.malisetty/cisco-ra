from socket import inet_ntoa
from struct import pack


def get_ip_netmask_from_cidr(data):

    # import ipdb; ipdb.set_trace();
    cidr = data['properties']['included'][0]['attributes']['layer3Attributes'][
        'additionalAttributes']['routes'][0]['ip_with_mask']
    bits = 0
    ip, netbits = cidr.split('/')

    for i in range(32 - int(netbits), 32):
        bits |= (1 << i)
    netmask = inet_ntoa(pack('>I', bits))
    data['properties']['included'][0]['attributes']['layer3Attributes'][
        'additionalAttributes']['routes'][0]['mask'] = netmask
    return data
