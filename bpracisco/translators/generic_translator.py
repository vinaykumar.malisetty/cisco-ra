import os
import logging
import re
import json
from twisted.internet import defer
from bpprov.translators.base import Translator
from rasdk.controller import Controller
from rasdk.rest import RasdkHttpError
from bpprov.components.rest import RestEndpoint
from bpprov.model import Config

log = logging.getLogger(__name__)

valid_ip_regex = \
    "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$" # noqa


MARKET_URL_ENV = 'MARKET_URL'
MARKET_URL = "http://bpocore/bpocore/market/api/v1"
MARKET_URL_DEV = os.environ.get(MARKET_URL_ENV)
TENANT_URL = "http://tron/tron/api/v1/tenants"
DOMAIN_URL = ''

SUCCESS_CODES = {200, 201}


class BaseUpdater(Translator):

    @defer.inlineCallbacks
    def get_tenant_from_tron(self):
        rest_ep = RestEndpoint(Config(None, "bpracisco/model"), {
            "debugLevel": 10,
            "hostname": "tron",
            "hostport": 443,
            "scheme": "http",
            "allowInsecureCertificates": True
        })
        response, body = yield rest_ep.request("GET",
                                               TENANT_URL,
                                               self.get_json_headers())
        tenant_uuid = None

        if response.code in SUCCESS_CODES:
            try:
                body = json.loads(body)
                for tenant in body["results"]:
                    if tenant["isMaster"] and tenant["isActive"]:
                        tenant_uuid = tenant["uuid"]
                        break
            except Exception:
                log.info("Could not decode response from BPO while retrieving tenants.")
        defer.returnValue(tenant_uuid)

    @defer.inlineCallbacks
    def get_nc_id(self, device_hostname):

        rest_ep = RestEndpoint(Config(None, "bpracisco/model"), {
            "debugLevel": 10,
            "hostname": "bpocore",
            "hostport": 443,
            "scheme": "http",
            "allowInsecureCertificates": True
        })
        resource_url = "/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct&" \
                       "p=label:{}*&q=discovered:true".format(device_hostname)

        if Controller.instance().args.dev:
            url = MARKET_URL_DEV + resource_url
        else:
            url = MARKET_URL + resource_url

        # tenant_id = yield self.get_tenant_from_tron()
        try:
            headers = {"bp-tenant-id": 'master', "Cyan-Internal-Request": "1"}
            response, body = yield rest_ep.request("GET",
                                                   url,
                                                   headers)

            if response.code in SUCCESS_CODES:
                resource = json.loads(body)
                if len(resource['items']) > 0:
                    defer.returnValue(resource['items'][0]['properties']['data']['attributes']['name'])
                else:
                    defer.returnValue(None)
        except Exception as e:
            log.error("Market GET resource failed : " + str(e))


class NCUpdater(BaseUpdater):
    '''Data related to NetworkConstruct can be updated'''

    schema = "NCUpdater.json"
    schema_base = os.path.join(os.path.dirname(os.path.abspath(__file__)), "schema")

    def process(self, route_data):
        session_id = self.config.get_session_id()
        try:
            session = Controller.instance().root.get('/sessions/' + session_id)
        except KeyError:
            raise RasdkHttpError(404, 'No session with id {}'.format(session_id))

        log.info("Route Data Updation is in progress ...")
        route_data.data.update({
            'session_id': session_id,
            'hostname': session['connection'].get('hostname'),
        })

        return route_data


class TPEUpdater_Put(BaseUpdater):

    schema = "TPEUpdater.json"
    schema_base = os.path.join(os.path.dirname(os.path.abspath(__file__)), "schema")

    @defer.inlineCallbacks
    def process(self, route_data):
        session_id = self.config.get_session_id()
        try:
            session = Controller.instance().root.get('/sessions/' + session_id)
        except KeyError:
            raise RasdkHttpError(404, 'No session with id {}'.format(session_id))
        tpe = route_data.data

        if tpe['properties']['data']['attributes']['structureType'] == "FTP":
            lag_id = tpe["properties"]["data"]["attributes"]["layerTerminations"][0][
                    "linkAggregationPackage"]["lagId"]
            all_ports = tpe['output']['add']
            tpe['properties']['data']['relationships']['owningServerTpeList'] = {}
            tpe['properties']['data']['relationships']['owningServerTpeList']['data'] = []
            for i in all_ports:
                tpe['properties']['data']['relationships']['owningServerTpeList']['data'].append(
                    {
                        "type": "tpes",
                        "id": i.replace("/", "-") + "-lag" + str(lag_id)
                    }
                )
        elif tpe['properties']['data']['attributes']['structureType'] == "CTPServerToClient":
            if tpe['properties']['data'].get('relationships') is None:
                device_hostname = session['connection'].get('hostname')
                nc_id = yield self.get_nc_id(device_hostname)
                tpe['properties']['data'].update(
                    {
                        "relationships": {
                            "networkConstruct": {
                                "data": {
                                    "id": nc_id,
                                    "type": "networkConstructs"
                                }
                            }
                        }
                    }
                )
            name = tpe["properties"]["data"]["id"]
            name_temp = name.split("-")
            if re.match('lag\d', name_temp[-1]):  # noqa
                name = name_temp[:-1]
            name1 = "-".join(name)
            tpe['properties']['data']['relationships'].update(
                {
                    "owningServerTpe": {
                        "data": {
                            "type": "tpes",
                            "id": name1
                        }
                    }
                }
            )
        defer.returnValue(route_data)


class TPEUpdater(BaseUpdater):

    schema = "TPEUpdater.json"
    schema_base = os.path.join(os.path.dirname(os.path.abspath(__file__)), "schema")

    @defer.inlineCallbacks
    def process(self, route_data):
        session_id = self.config.get_session_id()
        try:
            session = Controller.instance().root.get('/sessions/' + session_id)
        except KeyError:
            raise RasdkHttpError(404, 'No session with id {}'.format(session_id))
        device_hostname = session['connection'].get('hostname')
        nc_id = yield self.get_nc_id(device_hostname)

        r_data = route_data.data[:]
        for i in r_data:
            if not i:
                route_data.data.remove(i)

        for tpe in route_data.data:
            tpe['label'] = device_hostname + '_' + tpe['label']
            tpe['properties']['data'].update(
                {
                    "relationships": {
                        "networkConstruct": {
                            "data": {
                                "id": nc_id,
                                "type": "networkConstructs"
                            }
                        }
                    }
                })
            if tpe['properties']['data']['attributes']['structureType'] == "FTP":
                addAtributes = tpe['properties']['data']['attributes']['layerTerminations'][0]['linkAggregationPackage']
                if 'allPorts' in addAtributes:
                    if tpe['properties']['data']['relationships'].get('owningServerTpeList') is None:
                        tpe['properties']['data']['relationships']['owningServerTpeList'] = {}
                        tpe['properties']['data']['relationships']['owningServerTpeList']['data'] = []
                    lag_name = addAtributes["lagId"]
                    for i in addAtributes["allPorts"]:
                        match = re.search("([A-Za-z]+)(\S+)", i)  # noqa
                        final_name = match.group(1) + match.group(2).replace("/", "-")
                        tpe['properties']['data']['relationships']['owningServerTpeList']['data'].append(
                            {
                                "type": "tpes",
                                "id": final_name + "-lag" + lag_name
                            }
                        )
            elif tpe['properties']['data']['attributes']['structureType'] == "CTPServerToClient":
                finalname = ""
                name = tpe["properties"]["data"]["id"]
                name = name.split("-")[:-1]
                finalname = "-".join(name)
                tpe['properties']['data']['relationships'].update(
                    {
                        "owningServerTpe": {
                            "data": {
                                "type": "tpes",
                                "id": finalname
                            }
                        }
                    }
                )

        defer.returnValue(route_data)


class TPEUpdater_Create(BaseUpdater):

    schema = "TPEUpdater.json"
    schema_base = os.path.join(os.path.dirname(os.path.abspath(__file__)), "schema")

    @defer.inlineCallbacks
    def process(self, route_data):
        session_id = self.config.get_session_id()
        try:
            session = Controller.instance().root.get('/sessions/' + session_id)
        except KeyError:
            raise RasdkHttpError(404, 'No session with id {}'.format(session_id))
        device_hostname = session['connection'].get('hostname')
        nc_id = yield self.get_nc_id(device_hostname)
        tpe = route_data.data

        if tpe.get('label'):
            tpe['label'] = tpe['label']

        if tpe['properties']['data'].get('relationships') is None:
            tpe['properties']['data'].update(
                {
                    "relationships": {
                        "networkConstruct": {
                            "data": {
                                "id": nc_id,
                                "type": "networkConstructs"
                            }
                        }
                    }
                })
        else:
            tpe['properties']['data'].get('relationships').update(
                {
                    "networkConstruct": {
                        "data": {
                            "id": nc_id,
                            "type": "networkConstructs"
                        }
                    }
                })

        if 'structureType' in tpe['properties']['data']['attributes']:
            if tpe['properties']['data']['attributes']['structureType'] == "FTP":
                add_attributes = tpe['properties']['data']['attributes']['layerTerminations'][0][
                    'additionalAttributes']['linkAggregationPackage']
                if 'allPorts' in add_attributes:
                    if tpe['properties']['data']['relationships'].get('owningServerTpeList') is None:
                        tpe['properties']['data']['relationships']['owningServerTpeList'] = {}
                        tpe['properties']['data']['relationships']['owningServerTpeList']['data'] = []
                    lag_name = add_attributes["lagId"]
                    for i in add_attributes["allPorts"]:
                        match = re.search("([A-Za-z]+)(\S+)", i)  # noqa
                        final_name = match.group(1) + match.group(2).replace("/", "-")
                        tpe['properties']['data']['relationships']['owningServerTpeList']['data'].append(
                            {
                                "type": "tpes",
                                "id": final_name + "-lag" + str(lag_name)
                            }
                        )

            elif tpe['properties']['data']['attributes']['structureType'] == "CTPServerToClient":
                name = tpe["properties"]["data"]["id"]
                if name.count(".") == 1:
                    finalname = name.split(".")[0]
                    print("INSIDE", finalname)
                else:
                    name = name.split("-")[:-1]
                    finalname = "-".join(name)
                    print("FINAL NAME", finalname)
                tpe['properties']['data']['relationships'].update(
                    {
                        "owningServerTpe": {
                            "data": {
                                "type": "tpes",
                                "id": finalname
                            }
                        }
                    }
                )
        defer.returnValue(route_data)


def to_str(data):
    return {str(k): str(v) for k, v in data.items()}
