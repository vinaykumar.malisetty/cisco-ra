#!/usr/bin/env python
# Copyright(c) 2017, Ciena, Corp. All rights reserved.

import os

DEFAULT_CONFIG_DIRPATH = os.path.join(os.path.dirname(__file__), 'config/dev')
DEFAULT_PORT = 8080

RA_NAME = 'bpracisco'

BPPROV_MODEL_DIRPATH = os.path.join(os.path.dirname(__file__), 'model')

RESOURCES_DIR = os.path.join(BPPROV_MODEL_DIRPATH, 'resources')
RP_CONFIG = os.path.join(RESOURCES_DIR, 'rp_config.yaml')

DOMAIN_MANAGER_RP = False

# TYPE_GROUP is the general type of resource supported by the RA
TYPE_GROUP = 'Cisco'

# RESOURCE_TYPES lists the specific types of resources supported by the RA
RESOURCE_TYPES = ['ASR9K', 'ASR 9001', 'ASR 9901', 'ASR 9006', 'ASR 9010']

VENDOR = 'Ciena'

AUTHOR = 'Ciena'

STATIC_PATHS = {
    '/images/': os.path.join(os.path.dirname(__file__), 'model/graphics/images'),
}

ENDPOINTS = ['cli', 'kafka']

BPPROV_LOGGING_FILE_HANDLER = {
    'class': 'logging.handlers.RotatingFileHandler',
    'formatter': {
        'class': 'logging.Formatter',
        'format': '%(asctime)s %(levelname)s %(name)-12s %(message)s',
        'datefmt': None
        },
    'filename': '/bp2/log/{}.log',
    'maxBytes': 10000000,  # 10Megs
    'backupCount': 20,
}
