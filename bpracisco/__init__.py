from rasdk.conf import settings
from cryptography.hazmat.primitives.asymmetric import dsa


settings.configure('bpracisco.settings')

__version__ = '17.10.1'


def _check_dsa_parameters(parameters):
    from cryptography import utils
    if utils.bit_length(parameters.q) not in [160, 256]:
        raise ValueError("q must be exactly 160 or 256 bits long")
    if not (1 < parameters.g < parameters.p):
        raise ValueError("g, p don't satisfy 1 < g < p.")


dsa._check_dsa_parameters = _check_dsa_parameters
