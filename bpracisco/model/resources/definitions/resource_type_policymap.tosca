"$schema" = "http://cyaninc.com/json-schemas/tosca-lite-v1/definition-module#"
title = "Policy map resource type definition"
package = bpracisco
version = "1.0"
description = "This document defines policy map configuration"
authors = ["Vinay Kumar (vinaykum@partner.ciena.com)"]
imports {
  Root = tosca.resourceTypes.Root
  Container = tosca.capabilityTypes.Container
}
resourceTypes {
  PolicyMap {
    derivedFrom = Root
    title = "Policy map resource"
    description = """
    Policy map resource file
    """
    properties {
      device {
        type = string
        title = "Device"
        description = "Identifies the RA device containing the resource."
        optional = false
      }
      policy-map-name {
        type = string
        title = "Policy Map Name"
        description = "Policy Map Name"
        optional = false
      }
      class-info {
        type = array
        title = "Class Map Info"
        description = "Class Map Info"
        optional = true
        items {
            type = object
            properties {
                class-map-name {
                    type = string
                    title = "Class Map Name"
                    description = "Class Map Name"
                    optional = true
                }
                cir {
                    type = integer
                    title = "Commited Information Rate"
                    description = "Commited Information Rate"
                    optional = true
                }
                eir {
                    type = integer
                    title = "Excess Information Rate"
                    description = "Excess Information Rate"
                    optional = true
                }
                ebs {
                    type = integer
                    title = "Excess Burst Size"
                    description = "Excess Burst Size"
                    optional = true
                }
                cbs {
                    type = integer
                    title = "Comitted Burst Size"
                    description = "Comitted Burst Size"
                    optional = true
                }
                conform-action {
                    type = string
                    title = "Type of Action"
                    description = "Type of Action"
                    optional = true
                }
                exceed-action {
                    type = string
                    title = "Type of Action"
                    description = "Type of Action"
                    optional = true
                }
                violate-action {
                    type = string
                    title = "Type of Action"
                    description = "Type of Action"
                    optional = true
                }
                priority {
                    type = string
                    title = "Priority Level"
                    description = "Priority Level"
                    optional = true
                }
                qos {
                    type = string
                    title = "Quality of Service"
                    description = "Quality of Service"
                    optional = true
                }
                cos {
                    type = string
                    title = "COS"
                    description = "COS"
                    optional = true
                }
                bandwidth {
                    type = string
                    title = "Bandwidth Percentage"
                    description = "Bandwidth Percentage"
                    optional = true
                }
                shaper {
                    type = integer
                    title = "Shaper"
                    description = "Shaper"
                    optional = true
                }
            }
        }
      }
    }
    requirements {
        classmap {
            title = "Classmap"
            description = "Class-Map hosted on PolicyMap"
            type = Container
            resourceTypes = [ bpracisco.resourceTypes.ClassMap ]
        }
    }
  }
}