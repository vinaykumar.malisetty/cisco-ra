# The "id" is used to uniquely identify the RP implementation. The RP uses the
# identifier to identify itself when communicating with bpocore. A UUID or
# fully qualified URN is recommended.
id: "urn:ciena:bp:ra:bpracisco"

# The organization which maintains the RP. This is included when the RP is
# registered against bpocore. A URL is recommended.
vendor: "http://ciena.com"

# The vendor's license for the RP. This is included when the RP is registered
# against bpocore. A URL is recommended.
license: "http://ciena.com/license"

# Contact information for the maintainer/owner of the RP implementation. This
# is included when the RP is registered against bpocore.
contacts:
    - name: "Vinay Kumar"
      email: "vinaykum@partner.ciena.com"

# Maximum of 32 characters. Used for presentation purposes when a human
# readable name is required.
title: "Cisco"

# Used for presentation purposes when a long form human readable description
# of the RP is required.
description: ""

# The "version" should follow semantic versioning. Currently ignored.
version: "20.02.0"

# Location where RP specific definitions are stored. E.g, type definitions, UI
# schema, etc. This value can be absolute or relative to this file however it
# must be resolvable when deployed. If this value is null then no definitions
# will be onboarded.
definitions: "./definitions"

# Locacation where the UI schema files for the resource types and the domains are stored
# The UI schema files for resource types should be stored in the base directory of the specified
# location, while the UI schema files for the domains should be stored in the "domain-types"
# subdirectory of the location.
# This value can be absolute or relative to this file however it
# must be resolvable when deployed. If this value is null then no UI schema
# will be onboarded.
ui_schema: "./ui-schema"
ddui_schema: "./ddui-schema"

dependent_types:
  - 'tosca.resourceTypes.NetworkFunction'
  - 'tosca.resourceTypes.SessionProfile'
  - 'tosca.resourceTypes.NetworkConstruct'
  - 'tosca.resourceTypes.TPE'
  - 'tosca.resourceTypes.FRE'
  - 'tosca.resourceTypes.LLDP'

async_protocol_version: v2

# Domain definitions specify the types of domains supported by this RP.
# Developers must define their own domains.
# See the dtkdist repo's rpsdk docs (https://git.blueplanet.com/DevOpsToolkit/dtkdist/tree/master/docs) for more details.
# The RP should support at least one domain type.
domains:
  - id: "urn:ciena:bp:domain:bpracisco"
    title: "bpracisco"             # max 32 chars
    description: ""
    domain_settings:
        connection_status: true
    properties: []
    resource_types:
      - id: "bpracisco.resourceTypes.NetworkFunction"
        discoveryStrategy:
          strategyType: "async"
          pollingSettings:
            immediateDeletion: true
      - id: "bpracisco.resourceTypes.SessionProfile"
        discoveryStrategy:
          strategyType: "async"
          pollingMode: "noPolling"
          pollingSettings:
            immediateDeletion: true
      - id: "bpracisco.resourceTypes.NetworkConstruct"
        discoveryStrategy:
          strategyType: "async"
      - id: "tosca.resourceTypes.TPE"
        discoveryStrategy:
          strategyType: "async"
          pollingMode: "noPolling"
          pollingSettings:
            immediateDeletion: true
      - id: "tosca.resourceTypes.FRE"
        discoveryStrategy:
          strategyType: "async"
          pollingMode: "noPolling"
          pollingSettings:
            immediateDeletion: true
      - id: "tosca.resourceTypes.LLDP"
        discoveryStrategy:
          strategyType: "async"
          pollingMode: "noPolling"
          pollingSettings:
            immediateDeletion: true
      - id: "bpracisco.resourceTypes.PolicyMap"
        discoveryStrategy:
          strategyType: "async"
          pollingMode: "noPolling"
          pollingSettings:
            immediateDeletion: true
      - id: "cisco.resourceTypes.DIA"
          discoveryStrategy:
            strategyType: "async"
            pollingMode: "noPolling"
            pollingSettings:
              pollingIntervalMs: 60000
              immediateDeletion: true
              syncTimeoutMs: 180000
              verifyMissingList: false
          apiSettings:
            serverTimeoutMs: 180000

# Relationship definitions specify how relationships between resources managed
# by this RP are formed.
# See the dtkdist repo's rpsdk docs (https://git.blueplanet.com/DevOpsToolkit/dtkdist/tree/master/docs) for more details.
relationships:
  - sourceTypeId: "tosca.resourceTypes.TPE"                    # resource types involved in relationship
    targetTypeId: "bpracisco.resourceTypes.NetworkConstruct"
    capabilityName: tpes                                 # identifies endpoint names in relationship
    requirementName: networkConstruct
    relationshipTypeId: "tpe.relationshipTypes.NetworkConstruct"    # identifies type of relationship
    fieldKind: "provider"                                     # identicates what the field represent
    fieldLocator: "$.data.relationships.networkConstruct.data.id" # the field in the resource properties identifying the far end
  - sourceTypeId: "tosca.resourceTypes.TPE"               
    targetTypeId: "tosca.resourceTypes.TPE"
    capabilityName: clientTpes                                
    requirementName: owningServerTpe
    relationshipTypeId: "tpe.relationshipTypes.OwningServerTpe"   
    fieldKind: "provider"                                                        
    fieldLocator: "$.data.relationships.owningServerTpe.data.id"
  - sourceTypeId: "tosca.resourceTypes.TPE"
    targetTypeId: "tosca.resourceTypes.TPE"
    capabilityName: clientTpes
    requirementName: owningServerTpeList
    relationshipTypeId: "tosca.relationshipTypes.DependsOn"
    fieldKind: "provider"
    translate: false
    fieldLocator: "$.data.relationships.owningServerTpeList.data[*].id"
  - sourceTypeId: "tosca.resourceTypes.FRE"
    targetTypeId: "tosca.resourceTypes.TPE"
    requirementName: ctp
    capabilityName: clientFres
    relationshipTypeId: "fre.relationshipTypes.ClientTpes"
    fieldLocator: "$.included[*].relationships.tpes.data[*].id"
    translate: false
    fieldKind: "provider"
  - sourceTypeId: "bpracisco.resourceTypes.PolicyMap"
    targetTypeId: "bpracisco.resourceTypes.ClassMap"
    capabilityName: "policymap"
    requirementName: "classmap"
    relationshipTypeId: "tosca.relationshipTypes.MadeOf"
    fieldKind: "provider"
    fieldLocator: "class-info[*].class-map-name"
