#!/usr/bin/env python
# Copyright(c) 2016, Ciena Corporation. All rights reserved.

import logging
import json

log = logging.getLogger(__name__)
log.setLevel(logging.INFO)


def print_data(data):
    log.info("data is: %s", json.dumps(data, indent=4))
    return data


def set_show_device(data):
    resourceType = data['resourceType']

    if resourceType.find(" Series") > 0:
        data['resourceType'] = resourceType.replace(" Series", "")

    return data
