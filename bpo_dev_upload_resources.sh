#!/usr/bin/env bash

DEFINITIONS_DIR=/home/vagrant/model-definitions
rm -rf $DEFINITIONS_DIR
rm -rf pullrequest.json

function jq { python -c "import sys,json; print json.load(sys.stdin)$1"; }

BPOCORE_CID=$(docker ps | grep "bpocore-dev" | cut -f 1 -d ' ')
BPOCORE_IP=$(docker inspect bpocore-dev | jq '[0]["NetworkSettings"]["IPAddress"]')
MARKET_URL="http://$BPOCORE_IP:8181/bpocore/market/api/v1"
ASSETS_URL="http://$BPOCORE_IP:8181/bpocore/asset-manager/api/v1"
ssh-keygen -R $BPOCORE_IP
ssh-keyscan -H $BPOCORE_IP >> ~/.ssh/known_hosts

PUBLIC_KEY=`cat ~/.ssh/id_rsa.pub`
curl -s -H "Content-Type: application/json" \
    -d "{\"key\": \"$PUBLIC_KEY\"}" $ASSETS_URL/keys | python -m json.tool

git clone ssh://git@$BPOCORE_IP:22/home/git/repos/model-definitions $DEFINITIONS_DIR

cp /home/vagrant/shared/Uniti/NetworkModel/resources/definitions/types/tosca/* $DEFINITIONS_DIR/types/tosca

(cd $DEFINITIONS_DIR;
 git add types/tosca;
 git commit -m 'Add TPE-FRE Network Model and Common Model.';
 git push origin)

cat <<EOF > pullrequest.json
{
 "branch": "master",
 "title": "Add TPE-FRE Network Model and Common Model.",
 "comment": ""
}
EOF
curl -s -H "Content-Type: application/json" \
    -d @pullrequest.json $ASSETS_URL/areas/model-definitions/pullrequests \
    | python -m json.tool

rm -rf pullrequest.json
echo "Waiting 20 seconds for PR to merge"
sleep 20
