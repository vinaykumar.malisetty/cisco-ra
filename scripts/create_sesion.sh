#!/bin/bash

DEVICE_IP="10.92.19.71"

source dockerdev.sh

PRODUCT_ID=$(jcurl ${MARKET_URL}/products?q=resourceTypeId:bpracisco.resourceTypes.SessionProfile | jparse [\"items\"][0][\"id\"])

cat <<EOF > session_profile.json
{
"productId": "${PRODUCT_ID}",
"label": "cisco_ssh",
"discovered": false,
"properties": {
  "authentication": {
    "cli": {
      "password": "cisco",
      "username": "cisco"
    }    
  },
  "connection": {
    "cli": {
      "hostport": 22,
      "transport": "bpprov.components.cli_transport.SshTransport"
    }
  },    
  "typeGroup": "/typeGroups/Cisco"
}
}
EOF

jcurl -d @session_profile.json ${MARKET_URL}/resources | jpp

rm -f session_profile.json
