#!/usr/bin/env bash

source dockerdev.sh

PRODUCT_ID=$(jcurl ${MARKET_URL}/products?q=resourceTypeId:tosca.resourceTypes.TPE,providerProductId:urn:cyaninc:bp:product:bpracisco:tpe | jq [\"items\"][0][\"id\"])
DEVICE_RID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct\&label:NF_asr9k | jparse [\"items\"][0][\"providerResourceId\"])
NC_ID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct\&label:10.92.19.71 | jparse [\"items\"][0][\"id\"])

DOMAIN_TYPE=urn:ciena:bp:domain:bpracisco
DOMAIN_ID=$(jcurl $MARKET_URL/domains?q=domainType:$DOMAIN_TYPE | jparse [\"items\"][0][\"id\"])
FTP_RES=$(jcurl "$MARKET_URL/resources?domainId=$DOMAIN_ID&providerResourceId=$DEVICE_RID::GigabitEthernet0-0-0-16"| jparse [\"items\"][0][\"id\"])

cat <<EOF > create_tpe.json
{
    "productId": "${PRODUCT_ID}",
    "label": "10.92.19.71_GigabitEthernet0/0/0/16",
    "properties": {
        "device": "${DEVICE_RID}",
        "data": {
            "id": "GigabitEthernet0-0-0-16",
            "attributes": {
                "structureType" : "PTP",
                "userLabel": "GigabitEthernet0/0/0/16",
                "state": "IS",
                "layerTerminations": [{
                    "additionalAttributes": {
                        "description": "Test for TPE model-testing"
                    }
                }]
            }
        }
    }
}
EOF

jcurl -X PATCH -d @create_tpe.json $MARKET_URL/resources/${FTP_RES} | jpp
rm create_tpe.json
