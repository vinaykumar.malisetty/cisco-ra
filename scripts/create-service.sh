#!/usr/bin/env bash

source dockerdev.sh

PRODUCT_ID=$(jcurl ${MARKET_URL}/products?q=resourceTypeId:tosca.resourceTypes.FRE,providerProductId:urn:cyaninc:bp:product:bpracisco:fre | jq [\"items\"][0][\"id\"])
DEVICE_RID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct\&label:NF_asr9k | jparse [\"items\"][0][\"providerResourceId\"])

cat <<EOF > create_fre.json
{
    "productId": "$PRODUCT_ID",
    "label": "cisco_1",
    "properties": {
            "device": "$DEVICE_RID",
            "data": {
                "id": "cisco_1",
                "type": "fres",
                "attributes": {
                    "userLabel": "cisco_1"
                },
                "relationships": {
                    "endPoints": {
                        "data": [
                            {
                                "type": "endPoints",
                                "id": "endpoint1"
                            }
                        ]
                    }
                }
            },
            "included": [{
                    "id": "serviceIntentSpec1",
                    "type": "serviceIntentSpec",
                    "attributes": {
                        "layer3Attributes": {
                            "serviceType": "Internet_Service",
                            "additionalAttributes": {
                                "group_name": "group-1",
                                "domain_name": "domain-1"
                            }
                        }
                    }
                },
                {
                    "data": {
                        "type": "endpoints",
                        "id": "endpoint1",
                        "attributes": {
                            "role": "a"
                        },
                        "relationships": {
                            "tpes": {
                                "data": [{
                                    "type": "tpes",
                                    "attributes": {
                                        "locations": [{
                                            "neName": "10.92.19.71",
                                            "port": "100",
                                            "subport": "1",
                                            "vlan": "508",
                                            "userData": {
                                                "L2_transport" : true,
                                                "pop_value" : 1,
                                                "interface_type" : "Bundle-Ether",
                                                "description" : "Port for testing"
                                            }
                                          },
                                          {
                                            "neName": "10.92.19.71",
                                            "port": "20",
                                            "userData": {
                                                "ip_address" : "10.1.1.1",
                                                "mask" : "255.255.255.0",
                                                "interface_type" : "BVI",
                                                "description" : "Port for testing"
                                            }
                                        }]
                                    }
                                }]
                            }
                        }
                    }
                }
            ]
        }
}
EOF

jcurl -d @create_fre.json ${MARKET_URL}/resources | jpp
rm create_fre.json
