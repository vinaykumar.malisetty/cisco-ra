#!/usr/bin/env bash

source dockerdev.sh

PRODUCT_ID=$(jcurl ${MARKET_URL}/products?q=resourceTypeId:tosca.resourceTypes.FRE,providerProductId:urn:cyaninc:bp:product:bpracisco:fre | jq [\"items\"][0][\"id\"])
DEVICE_RID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct\&label:NF | jparse [\"items\"][0][\"providerResourceId\"])
NC_ID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct | jparse [\"items\"][0][\"id\"])

cat <<EOF > create_fre.json
{
    "productId": "$PRODUCT_ID",
    "label": "FRE_Bundle-Ether10.3217",
    "properties": {
            "device": "$DEVICE_RID",
            "data": {
                "id": "Bundle-Ether10.3217",
                "type": "fres",
                "attributes" :{
                    "description": "Testing",
                    "additionalAttributes": {
                        "ipv4Address" : "192.168.192.189/30",
                        "mtu" : "1518",
                        "vlan": "3217",
                        "totalBandwidth" : "0",
                        "totalBandwidthUnits" : "kbit",
                        "policyPayload": {
                            "policy_ingress": "input",
                            "policy_egress": "output",
                            "input_capacity": "50M",
                            "output_capacity": "50M"
                        },
                        "accessGroupName": "SSH",
                        "direction": "ingress"
                    }
                },
                "relationships": {
                    "networkConstruct": {
                        "data": {
                            "id": "${NC_ID}",
                            "type": "networkConstructs"
                        }
                    }
                }
            },
            "included": [
                {
                    "id": "FRE_Bundle-Ether10.3217",
                    "type": "endpoints",
                    "attributes": {
                        "role": "a",
                        "directionality": "bidirectional"
                    },
                    "relationships": {
                        "tpes": {
                            "data": [
                                {
                                    "type": "tpes",
                                    "id": "Bundle-Ether10"
                                }
                            ]
                        }
                    }
                }
            ]

    }
}
EOF

jcurl -d @create_fre.json ${MARKET_URL}/resources | jpp
rm create_fre.json
