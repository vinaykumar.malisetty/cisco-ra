#!/usr/bin/env bash

source dockerdev.sh

PRODUCT_ID=$(jcurl ${MARKET_URL}/products?q=resourceTypeId:tosca.resourceTypes.TPE,providerProductId:urn:cyaninc:bp:product:bpracisco:tpe | jq [\"items\"][0][\"id\"])
DEVICE_RID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct\&label:NF | jparse [\"items\"][0][\"providerResourceId\"])
NC_ID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct\&label:172.18.50.146 | jparse [\"items\"][0][\"id\"])

DOMAIN_TYPE=urn:ciena:bp:domain:bpracisco
DOMAIN_ID=$(jcurl $MARKET_URL/domains?q=domainType:$DOMAIN_TYPE | jparse [\"items\"][0][\"id\"])
CTP_RES=$(jcurl "$MARKET_URL/resources?domainId=$DOMAIN_ID&providerResourceId=$DEVICE_RID::Bundle-Ether10.3217"| jparse [\"items\"][0][\"id\"])

cat <<EOF > create_tpe.json
{
  "productId": "${PRODUCT_ID}",
  "label": "Bundle-Ether10.3217",
  "properties": {
    "device": "${DEVICE_RID}",
    "data": {
      "id": "Bundle-Ether10.3217",
      "attributes": {
        "layerTerminations": [
          {
            "additionalAttributes": {
              "portDescription": "TestRun",
              "encapsulation": {
                "vlan": 3219
              },
              "accessGroupName": "Temp_DenyRFC1918_MCPSS",
              "direction": "egress"
            }
          }
        ]
      }
    }
  }
}
EOF

jcurl -X PATCH -d @create_tpe.json $MARKET_URL/resources/${CTP_RES} | jpp
rm create_tpe.json
