#!/bin/bash

DEVICE_IP="10.92.19.71"

source dockerdev.sh

PRODUCT_ID=$(jcurl ${MARKET_URL}/products?q=resourceTypeId:bpracisco.resourceTypes.NetworkConstruct | jparse [\"items\"][0][\"id\"])
SESSION_PROFILE_ID=$(jcurl $MARKET_URL/resources?resourceTypeId=bpracisco.resourceTypes.SessionProfile\&q=label:cisco_ssh | jparse [\"items\"][0][\"id\"])
echo "${SESSION_PROFILE_ID}"
cat <<EOF > network_function.json
{
"productId": "${PRODUCT_ID}",
"label": "NF_asr9k",
"discovered": false,
"properties": {
    "sessionProfile": "${SESSION_PROFILE_ID}",
    "ipAddress": "${DEVICE_IP}"
}
}
EOF

jcurl -d @network_function.json ${MARKET_URL}/resources | jpp

rm -f network_function.json
