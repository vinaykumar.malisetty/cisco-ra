#!/usr/bin/env bash

source dockerdev.sh

PRODUCT_ID=$(jcurl ${MARKET_URL}/products?q=resourceTypeId:tosca.resourceTypes.TPE,providerProductId:urn:cyaninc:bp:product:bpracisco:tpe | jq [\"items\"][0][\"id\"])
DEVICE_RID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct | jparse [\"items\"][0][\"providerResourceId\"])
NC_ID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct | jparse [\"items\"][0][\"id\"])
cat <<EOF > create_tpe.json
{
  "productId": "${PRODUCT_ID}",
  "label": "Bundle-Ether10.3217",
  "properties": {
    "device": "${DEVICE_RID}",
    "data": {
      "id": "Bundle-Ether10.3217",
      "type": "tpes",
      "relationships": {
        "networkConstruct": {
          "data": {
            "id": "${NC_ID}",
            "type": "networkConstructs"
          }
        }
      },
      "attributes": {
        "locations": [
          {
            "port": "Bundle-Ether10.3217"
          }
        ],
        "stackDirection": "bidirectional",
        "structureType": "CTPServerToClient",
        "userLabel": "Bundle-Ether10.3217",
        "state": "IS",
        "layerTerminations": [
          {
            "additionalAttributes": {
              "ipv4Address": "170.249.149.33/30",
              "portDescription": "/INT/138062//UIF/",
              "encapsulation": {
                "vlan": 3217
              },
              "policyPayload": {
                "policy_name": "service-policy",
                "policy_ingress": "input",
                "policy_egress": "output",
                "input_capacity": "50M",
                "output_capacity": "50M"
              },
              "accessGroupName": "SSH",
              "direction": "ingress"
            },
            "layerRate": "ETHERNET",
            "structureType": "exposed lone cp",
            "terminationState": "layer termination permanently terminated",
            "active": true
          }
        ],
        "active": true
      }
    }
  }
}
EOF
jcurl -d @create_tpe.json ${MARKET_URL}/resources | jpp
rm create_tpe.json