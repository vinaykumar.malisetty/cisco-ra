#!/usr/bin/env bash

source dockerdev.sh

PRODUCT_ID=$(jcurl ${MARKET_URL}/products?q=resourceTypeId:tosca.resourceTypes.TPE,providerProductId:urn:cyaninc:bp:product:bpracisco:tpe | jq [\"items\"][0][\"id\"])
DEVICE_RID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct\&label:NF_asr9k | jparse [\"items\"][0][\"providerResourceId\"])
NC_RPID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct\&label:10.92.19.71 | jparse [\"items\"][0][\"providerResourceId\"])
NC_ID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=bpracisco.resourceTypes.NetworkConstruct\&label:10.92.19.71 | jparse [\"items\"][0][\"id\"])

cat <<EOF > create_tpe.json
{
    "productId": "${PRODUCT_ID}",
    "label": "10.92.19.71_Bundle-Ether89",
    "properties": {
        "device": "${DEVICE_RID}",
        "data": {
            "id" : "Bundle-Ether89",
            "type": "tpes",
            "relationships" : {
                "networkConstruct": {
                    "data": {
                        "id": "${NC_ID}",
                        "type": "networkConstructs"
                    }
                }
            },
            "attributes": {
                "locations": [
                {
                    "port": "Bundle-Ether89"
                }],
                "structureType" : "FTP",
                "userLabel": "Bundle-Ether89",
                "state": "IS",
                "layerTerminations": [{
                    "additionalAttributes": {
                        "lagId": "89",
                        "lag_list": ["GigabitEthernet0/0/0/17", "GigabitEthernet0/0/0/18"],
                        "ipv4_address": "10.11.1.1/24",
                        "description": "Test for TPE model",
                        "ipv6_address": ["10:10::89:5/16", "10:10::92:5/32"]
                    }
                }]
            }
        }
    }
}
EOF

jcurl -d @create_tpe.json ${MARKET_URL}/resources | jpp
rm create_tpe.json
