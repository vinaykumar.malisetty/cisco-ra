#!/bin/bash

source dockerdev.sh

RP_ID=$(jcurl $MARKET_URL/resource-providers?q=domainType:urn:ciena:bp:domain:bpracisco | jparse [\"items\"][0][\"id\"])
cat <<EOF > /tmp/domain.json
{
    "accessUrl": "http://localhost:9192",
    "address": {
        "city": "Parker",
        "country": "US",
        "latitude": 33.055397033691406,
        "longitude": -96.62194061279297,
        "state": "TX",
        "street": "",
        "zip": "75002"
    },
    "domainType": "urn:cyaninc:bp:domain:ciscong",
    "operationMode": "normal",
    "properties": {},
    "rpId": "$RP_ID",
    "title": "Cisco"
}
EOF

jcurl -d @/tmp/domain.json $MARKET_URL/domains | jpp
