#!/bin/bash

source dockerdev.sh

INTERFACE_ID="Bundle-Ether10.3217"

RESOURCE_ID=$(jcurl ${MARKET_URL}/resources?resourceTypeId=tosca.resourceTypes.FRE\&q=label:$INTERFACE_ID| jparse [\"items\"][0][\"id\"])

jcurl -X DELETE ${MARKET_URL}/resources/${RESOURCE_ID}
