#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from setuptools import setup, find_packages
import os
import json

# Allow to run setup.py from another directory.
os.chdir(os.path.dirname(os.path.abspath(__file__)))

version = '20.02.0'

with open('version.json') as v:
    data = json.load(v)
setup(
    name=data['name'],
    version=data['version'],
    packages=find_packages(
        exclude=(
            '.*',
            'EGG-INFO',
            '*.egg-info',
            '_trial*',
            '*.tests',
            '*.tests.*',
            'tests.*',
            'tests',
            'examples.*',
            'examples*',
            )
        ),
    include_package_data=True,
    install_requires=[
        'bpprov[kafka]',
        'rasdk',
        'rpsdk[ra]',
        ],
    entry_points={
        'console_scripts': [
            'bpracisco = bpracisco.main:main',
            ]
        },
    tests_require=[
        'mock',
        'nose',
        ],
    test_suite='nose.collector',
)
