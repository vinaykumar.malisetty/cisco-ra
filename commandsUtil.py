
def inspect(data, pretty=True):
    import json
    print("***********data is: {}".format(json.dumps(data, indent=4 if pretty else None)))
    return data

def getRouteOption(data):
    log.info("Data: {}".format(data))
    if 'properties' in data:
        prop = data.get('properties', "")
    else:
        prop = data
    data['routeInfoDetail'] = str(prop.get('routing', "").get('routingOption', ""))

    log.info("Data: {}".format(data))
    return data

def returnSuccess(data):
    return True
